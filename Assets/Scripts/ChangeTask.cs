using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeTask : MonoBehaviour
{
    public GameObject screenTask1;
    public GameObject screenTask2;
    private GameObject currentscreen;
    
    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void OnButtonClick()
    {
        if (screenTask1.activeSelf)
        {
            screenTask1.SetActive(false);
            screenTask2.SetActive(true);
        }
        else if (screenTask2.activeSelf == false)
        {
            screenTask2.SetActive(false);
            screenTask1.SetActive(true);
        }
    }
}
