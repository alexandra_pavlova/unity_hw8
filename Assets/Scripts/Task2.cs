using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task2 : MonoBehaviour
{
    public float Speed;
    public bool Go;
    private Vector3 target;
    public float passDistance;
    private float currentDistance;

    private static GameObject[] Runners;
    private static Transform[] RunnersInf;
    private int currentRunnerNum;
    private GameObject currentRunner;
    private GameObject nextRunner;
    private Vector3 finishVector = new Vector3(3f, 3f, 3f);
    public int numberOfRunners;
    public GameObject runner;

    void Start()
    {
        Runners = new GameObject[numberOfRunners];
        for (int i = 0; i < numberOfRunners; ++i)
        {
            Runners[i] = Instantiate(runner);
            Runners[i].SetActive(true);
        }

        int a = 0;
        int b = 0;
        int c = 0;

        RunnersInf = new Transform[numberOfRunners];
        for (int i = 0; i < numberOfRunners; ++i)
        {
            RunnersInf[i] = runner.transform;
            RunnersInf[i].position = new Vector3(a, b, c);
            Runners[i].transform.position = RunnersInf[i].position;

            a += 3;
            b += 3;
            c += 3;
        }

        currentRunnerNum = 0;
        currentRunner = Runners[currentRunnerNum];
        nextRunner = Runners[currentRunnerNum + 1];
        target = nextRunner.transform.position;

    }

    void Update()
    {
        float x1 = currentRunner.transform.position.x;
        float x2 = nextRunner.transform.position.x;
        float y1 = currentRunner.transform.position.y;
        float y2 = nextRunner.transform.position.y;
        float z1 = currentRunner.transform.position.z;
        float z2 = nextRunner.transform.position.z;

        Vector3 vec = new Vector3(x1 - x2, y1 - y2, z1 - z2);
        currentDistance = vec.magnitude;

        if (Go)
        {
            if (currentRunnerNum < numberOfRunners - 1)
            {
                currentRunner.transform.position = Vector3.MoveTowards(currentRunner.transform.position, target,
                    Time.deltaTime * Speed);
                if ((currentDistance <= passDistance) && (currentRunnerNum < numberOfRunners - 2))
                {
                    currentRunnerNum += 1;
                    currentRunner = Runners[currentRunnerNum];
                    nextRunner = Runners[currentRunnerNum + 1];
                    target = nextRunner.transform.position;
                }
            }
        }

        if (currentRunner.transform.position == Runners[numberOfRunners - 1].transform.position)
        {
            print("End of Race!");
        }
        
    } 
}
