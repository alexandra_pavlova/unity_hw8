using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VectorExample : MonoBehaviour
{
    public float Speed;
    public bool Go;
    private bool Forward;
    private Vector3 target;

    private static Vector3[] Points;
    private int currentPoint;
    public int numberOfPoints;
    
    void Start()
    {
        int a = 0;
        int b = 0;
        int c = 0;
        
        Points = new Vector3[numberOfPoints + 1];
        for (int i = 0; i < numberOfPoints; ++i)
        {
            Points[i] = new Vector3(a,b,c);
            a += 1;
            b += 1;
            c += 1;
            print(Points[i] + "\n");
        }

        Forward = true;
        currentPoint = 0;
        target = Points[currentPoint];
        transform.LookAt(target);
    }

    void Update()
    {
        transform.Rotate(0,0,1); // вращение

        if (Go)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * Speed);
            if (transform.position == target)
            {
                print(currentPoint + " target reached");
                if (Forward)
                {
                    if (currentPoint == (numberOfPoints - 1))
                    {
                        Forward = false;
                    }
                    else
                    {
                       currentPoint += 1;
                       target = Points[currentPoint]; 
                       transform.LookAt(target);
                    }
                }
                else
                {
                    if (currentPoint == 0)
                    {
                        Forward = true;
                    }
                    else
                    {
                        currentPoint -= 1;
                        target = Points[currentPoint];
                        transform.LookAt(target);
                    }
                }
            }
        }
    }
}
